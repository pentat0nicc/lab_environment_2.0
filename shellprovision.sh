#!/bin/bash

#add a ansible user account to all machines, and copy over keys

useradd -c "ansible" ansible
mkdir /home/ansible/.ssh
chmod 700 /home/ansible/.ssh
chown ansible:ansible /home/ansible/.ssh
touch /home/ansible/.ssh/authorized_keys
cat /vagrant/keys/id_rsa.pub > /home/ansible/.ssh/authorized_keys
chown ansible:ansible /home/ansible/.ssh/authorized_keys
chmod 600 /home/ansible/.ssh/authorized_keys
cp /vagrant/keys/id_rsa /home/ansible/.ssh/id_rsa
chown ansible:ansible /home/ansible/.ssh/id_rsa

#make ansible sudo no passwd
echo "ansible        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

#cat Vagrantfile |grep ip |awk '{ print $4, $1 }' | tr -d \"  |sed 's/.vm.network//'
#this file takes a vagrant file and create a host file to copied over to linux vms

cat /vagrant/Vagrantfile | \
    grep ip | \
    awk '{ print $4, $1 }' | \
    tr -d \" | \
    sed 's/.vm.network//' >>  /etc/hosts
